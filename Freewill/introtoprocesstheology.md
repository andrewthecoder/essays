# Introduction to Process Theology

Classical theism offers us the concept of an asymmetric relationship with G-d.  
G-d creates, but his creatures have no creative power. Charles Hartshorne  
stated it something like "one wholly uncreated, the other wholly uncreative."  
In this view, it is completely out of the realm of the creatures' ability  
to make a difference to G-d. Thomas Aquinas explained such an asymmetry by  
expanding on Aristotle's concepts of "pure act" and "unmoved mover," saying  
that the relation from G-d to His creatures is real; it makes all the  
difference to them, whereas the creatures' relation to G-d is rational;  
it is in the mind only, as the existence of the creatures makes no difference  
to the being of G-d.


This begs the question: if G-d is "pure act," meaning that anything G-d  
could be, He already is, is there any potential in G-d for any type of change?  
To say that G-d is the "unmoved mover" is to imply that He moves others  
but is unmoved by any other. This is to say that G-d is impassible and  
literally without feeling or emotion.


This is a paradox that borders on incoherence. It says that G-d is  
all-knowing of a contingent world, yet nothing in Him can be other than it is.  
These conditions contradict one another, for any contingent event could be  
otherwise. As an example, consider that a marathoner at a certain time and  
place is sleeping rather than running. But if the event could be reversed,  
G-d's knowledge of the event would also be reversed. This is not to say that  
G-d might have been ignorant of something; rather, the thing G-d knows might  
have been different. So, G-d in his infallibility necessarily knows whatever  
exists; however, it does not follow that what exists is necessary unless  
one adds the premise that nothing in G-d can be other than it is.


The conclusion here, as far as I can tell, is that a G-d without contingent  
aspects, knowing a contingent world, is logically impossible.
